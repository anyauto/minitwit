package link.anycar.minitwit.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by voyo on 2018/6/26.
 */
public class Message {

    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd @ HH:mm");

    private long id;
    private long userId;
    private String username;
    private String text;
    private Date pubDate;
    private String pubDateStr;
    private String gravatar;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
        if (pubDate != null) {
            setPubDateStr(format.format(pubDate));
        } else {
            pubDateStr = null;
        }
    }

    public String getPubDateStr() {
        return pubDateStr;
    }

    public void setPubDateStr(String pubDateStr) {
        this.pubDateStr = pubDateStr;
    }

    public String getGravatar() {
        return gravatar;
    }

    public void setGravatar(String gravatar) {
        this.gravatar = gravatar;
    }
}
